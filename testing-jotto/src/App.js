import React, { Component } from 'react';
import { connect } from 'react-redux';
import './App.css';
import GuessedWords from './components/guessedWords';
import Input from './components/Input';
import PlayAgain from './components/playAgain';
import WordDisplay from './components/wordDisplay';
import { getSecretWord, setDefaultAsterisks } from './actions/index';
import { convertToAsterisks } from './helpers/index';

class App extends Component {
  componentDidMount() {
    this.props.getSecretWord();
    const wordToGuess = convertToAsterisks(this.props.wordToGuess);
    this.props.setDefaltAsterisksDisplay(wordToGuess);
  }

  render () {
    return (
      <section className="container" data-test="app-render">
        <h1>Jotto - word guess game</h1>
        <WordDisplay />
        <Input />
        <GuessedWords />
        <PlayAgain />
      </section>
    )
  }
}

const mapSateToProps = (state) => {
  return {
    wordToGuess: state.successReducer.wordToGuess
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getSecretWord: () => {
      dispatch(getSecretWord());
    },
    setDefaltAsterisksDisplay: (wordToGuess) => {
      dispatch(setDefaultAsterisks(wordToGuess))
    }
  }
}

export default connect(mapSateToProps, mapDispatchToProps)(App);
