import React, {Component} from 'react';
import { connect } from 'react-redux';
import { replayGameAction } from '../actions/index';

class PlayAgain extends Component {
  replayGame = () => {
    this.props.replayGame();
  }

  render() {
    return  (
      <section data-test="play-again-section">
        {this.props.success ? (<button 
          id="guess-word-submit" 
          data-test="play-again-button" 
          className="btn btn-primary mb-2"
          onClick={ (event) => this.replayGame() }
        >
          Play Again
        </button>) : null }
      </section>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    fullState: state,
    success: state.successReducer.success
  }
}

const mapDispatchToProps = (dispatch) => {
  return  {
    replayGame : () => {
      dispatch(replayGameAction())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlayAgain);
