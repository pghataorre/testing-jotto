import React, {Component} from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import GuessingResults from './guessingResults';

class GuessedWords extends Component {
  render() {
    let contents;

    if (this.props.guessedWords.length === 0) {
      contents = (
        <span data-test="guess-instructions">Try and guess the word !</span>
      )
    } else {
      const guessedWords = this.props.guessedWords;

      contents = (
        <span data-test="guess-words-list">
          <h3>Guessed words</h3>
          <table className="table table-sm">
            <thead className="thead-light">
              <tr>
                <th>Guess</th>
                <th>Matching Letters</th>
              </tr>
            </thead>
            <tbody>
              <GuessingResults guessedWords={ guessedWords } />
            </tbody>
          </table>
        </span>
      )
    }

    return (
      <section data-test="guessed-words-section">
        { this.props.success ?  null : contents }
      </section>
    )
  }
}

GuessedWords.propTypes = {
  guessedWords: PropTypes.arrayOf(
    PropTypes.shape({
      guessedWord: PropTypes.string.isRequired,
      letterMatchCount: PropTypes.number.isRequired
    })
  ).isRequired
};

const mapStateToProps = (state) => {
  return {
    success: state.successReducer.success,
    guessedWords: state.failedReducer.guessedWords
  };
}

export default connect(mapStateToProps)(GuessedWords);

