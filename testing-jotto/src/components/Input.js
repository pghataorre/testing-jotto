import React, { Component } from 'react';
import { connect } from 'react-redux';
import Congrats from  './congrats'; 
import { setAttemptGuess } from '../actions/index';

class Input extends Component {
  matchAttemptGuess = (event) => {
    const guessWord = event.currentTarget.elements[0].value;
    if (guessWord) {
      this.props.attemptGuess(guessWord);
    }

    event.preventDefault();
  }

  render() {
    let contents;

    contents = this.props.success
      ? (<div><Congrats success={this.props.success} /></div>)
      : (<form type="POST" className="form-inline" data-test="guess-word-form" onSubmit={(event) => this.matchAttemptGuess(event) }>
        <input type="text" className="mb-2" name="guess-word" id="guess-word" data-test="guess-word-input" placeholder="Enter word to guess"/>
        <button type="sbumit" id="guess-word-submit" data-test="guess-word-submit" className="btn btn-primary mb-2">Submit word</button>
      </form>)

   return (
    <section data-test="input-container">
      {contents}
    </section>
    )
  }  
}

const mapStateToProps = (state) => {
  return {
    success: state.successReducer.success,
    secretWord: state.successReducer.wordToGuess
   };
}

const mapDispatchToProps = (dispatch) => {
  return {
    attemptGuess: (guessWord) => {
      dispatch(setAttemptGuess(guessWord));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Input);
