import React from 'react';
import { connect } from 'react-redux';

const WordDisplay = (props) => {
  return (
    <section data-test="display-word">
      <div className="word-asterisks" data-test="word-asterisks">{ props.displayAsterisks }</div>
    </section>
  )
}

const mapStateToProps = (state) => {
  return {
    displayAsterisks: state.successReducer.asterisksDisplay
  }
}

export default connect(mapStateToProps)(WordDisplay);