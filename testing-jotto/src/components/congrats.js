import React from 'react';
import propTypes from 'prop-types';

const Congrats = (props) => {
  return (
    <section data-test="congrats-section">
      <div>
        {props.success ? (<div className="alert alert-success" data-test="congrats-message">Congrats you've won !</div>) : null}
      </div>
    </section>
  )
}

export default Congrats;

Congrats.propTypes = {
  success: propTypes.bool.isRequired
};