import React, { Component } from 'react';

class GuessingResults extends Component {
  render () {
    const guessedWords = this.props.guessedWords;

    if (guessedWords.length) {
      return guessedWords.map((item, index) => {
        return (<tr data-test="guessed-word-row" key={ index }>
          <td data-test="guessed-word-col">{ item.guessedWord }</td>
          <td data-test="letterMatchCount-col">{ item.letterMatchCount }</td>
        </tr>)
        }) 
    } else {
      return null;
    }
  }
}

export default GuessingResults;
