import React from 'react';
import { createStore } from 'redux';
import { mount } from 'enzyme';
import WordDisplay from '../components/wordDisplay';
import rootReducer from '../reducers/rootReducer';
import initialState from '../initialState/initialState';

let wrapper, store;

const setUp = (state) => {
  store = createStore(rootReducer, state);
  wrapper = mount(<WordDisplay store={store} />);

  return wrapper;
} 

describe('WordDisplay component renders', () => {
  test('Should render as default', () => {
    wrapper = setUp();
    expect(wrapper.find(`[data-test='display-word']`).length).toBe(1);
  });

  test(`Should render as default with a number of '*'`, () => {
    const expectedOutput = '******';
    initialState.asterisksDisplay = '******'
    wrapper = setUp(initialState);
    expect(wrapper.find(`[data-test='word-asterisks']`).length).toBe(1);
    expect(wrapper.find(`[data-test='word-asterisks']`).text().trim()).toBe(expectedOutput);
  });
});