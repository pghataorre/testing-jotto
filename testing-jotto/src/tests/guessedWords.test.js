import React from 'react';
import { createStore } from 'redux';
import {shallow, mount} from 'enzyme';
import GuessedWords from '../components/guessedWords';
import rootReducer from '../reducers/rootReducer';
import initialState from '../initialState/initialState';
import checkProptypes from 'check-prop-types';

const defaultProps = { 
  guessedWords: [
    { guessedWord: 'train', letterMatchCount: 3 }
  ]
};

let store, wrapper

const setup = (state, useMout) => {
  store = createStore(rootReducer, state);
  return useMout 
    ? mount(<GuessedWords store={store} />)
    : shallow(<GuessedWords store={store} />);
};

describe('GuessedWords component', () => {
  test('should render on page', () => {
    wrapper = setup(initialState).dive().dive();
    expect(wrapper.find(`[data-test='guessed-words-section']`).length).toBe(1);
  });

  test('should render intial instruction', () => {
    wrapper = setup(initialState).dive().dive();    expect(wrapper.find(`[data-test='guess-instructions']`).length).toBe(1);
  });

  test('should render guessed words list - USED MOUNT', () => {
    const guessedWords = [
      { guessedWord: 'train', letterMatchCount: 3 },
      { guessedWord: 'tiger', letterMatchCount: 2 },
      { guessedWord: 'snow', letterMatchCount: 0 }
    ];

    initialState.guessedWords = guessedWords;
    wrapper = setup(initialState).dive().dive();

    expect(wrapper.find(`[data-test='guess-words-list']`).length).toBe(1);
  });
});


describe('GuessedWords component state with guess words attempts', () => {
  test('should render guessed words list', () => {
    const guessedWords = [
      { guessedWord: 'train', letterMatchCount: 3 },
      { guessedWord: 'tiger', letterMatchCount: 2 },
      { guessedWord: 'snow', letterMatchCount: 0 }
    ];
    const useMount = true;

    initialState.guessedWords = guessedWords;
    wrapper = setup(initialState, useMount);
    expect(wrapper.find(`[data-test='guessed-word-row']`).length).toBe(3);
  });
});


describe('GuessedWords component state when the secret word is guessed', () => {
  test('should render nothing', () => {
    initialState.success = true;
    wrapper = setup(initialState).dive().dive();
    expect(wrapper.find(`[data-test='guess-words-list']`).length).toBe(0);
  });
});


// ==========================================================
// EXAMPLES OF CHECKING PROPS -- HAVE CHANGED TO USE REDUX;
// ==========================================================

// describe('GuessedWords component', () => {
//   test('should render with correct props', () => {
//     const propsError = checkProptypes(GuessedWords.propTypes, defaultProps, 'prop', GuessedWords.name);
//     expect(propsError).toBeUndefined();
//   });

//   test('should error with wrong props', () => {
//     const wrongLetterMatchCountProps = { guessedWords: 
//       [{ guessedWord: 'train', letterMatchCount: 'TEST WRONG PROP' }]
//     };

//     const propsError = checkProptypes(GuessedWords.propTypes, wrongLetterMatchCountProps, 'prop', GuessedWords.name);
//     expect(propsError).toBe('Failed prop type: Invalid prop `guessedWords[0].letterMatchCount` of type `string` supplied to `GuessedWords`, expected `number`.');
//   });

//   test('should error with wrong props', () => {
//     const wrongLetterMatchCountProps = { guessedWords: 
//       [{ guessedWord: 2, letterMatchCount: 3 }]
//     };

//     const propsError = checkProptypes(GuessedWords.propTypes, wrongLetterMatchCountProps, 'prop', GuessedWords.name);
//     expect(propsError).toBe('Failed prop type: Invalid prop `guessedWords[0].guessedWord` of type `number` supplied to `GuessedWords`, expected `string`.');
//   });
// });


