import React from 'react';
import { createStore } from 'redux';
import { mount } from 'enzyme';
import Input  from '../components/Input';
import rootReducer from '../reducers/rootReducer';
import initialState from '../initialState/initialState';

let wrapper, store;

const setUp = (state) => {
  store = createStore(rootReducer, state);
  wrapper = mount(<Input store={store} />);

  return wrapper;
}

describe('Input component renders', () => {
  test('Renders on page', () => {
    wrapper = setUp(initialState);
    expect(wrapper.find(`[data-test='input-container']`).length).toBe(1);
    expect(wrapper.find(`[data-test='guess-word-form']`).length).toBe(1);
    expect(wrapper.find(`[data-test='guess-word-submit']`).length).toBe(1);
  });

  test('Renders on page with NO INPUT when {success: true}', () => {
    const mockState = {
      successReducer: {
        success: true,
        guessedWords: [],
        wordToGuess: 'testing'
      }
    };

    wrapper = setUp(mockState);
    expect(wrapper.find(`[data-test='guess-word-form']`).length).toBe(0);
    expect(wrapper.find(`[data-test='congrats-section']`).length).toBe(1);
  });
});

// console.log(`\n  ================================= \n  wrapper === ${wrapper.debug()} \n ================================= \n`);
