import React from 'react';
import {shallow} from 'enzyme';
import Congrats from '../components/congrats';
import checkProptypes from 'check-prop-types';

describe('Congrats component', () => {
  test('should render', () => {
    const wrapper = shallow(<Congrats success={true}/>);
    expect(wrapper.find(`[data-test='congrats-section']`).length).toBe(1);
    expect(wrapper.find(`[data-test='congrats-message']`).length).toBe(1);
  });
});

describe('Congrats component should render', () => {
  test('with correct messages when props success: false', () => {
    const wrapper = shallow(<Congrats success={false} />);
    expect(wrapper.find(`[data-test='congrats-section']`).length).toBe(1);
    expect(wrapper.find(`[data-test='congrats-message']`).length).toBe(0)
  });

  test('with correct propTypes', () => {
    const exptectedProps  = {success: false};
    const propsError = checkProptypes(Congrats.propTypes, exptectedProps, 'prop', Congrats.name);
    expect(propsError).toBeUndefined();
  });

  test('with wrong propTypes', () => {
    const exptectedProps  = {success: 'STRING'};
    const propsError = checkProptypes(Congrats.propTypes, exptectedProps, 'prop', Congrats.name);
    expect(propsError).toBe('Failed prop type: Invalid prop `success` of type `string` supplied to `Congrats`, expected `boolean`.');
  });

});
