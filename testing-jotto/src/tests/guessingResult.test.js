import React from 'react';
import { shallow } from 'enzyme';
import GuessingResults from '../components/guessingResults';

describe('GuessingResults renders', () => {
  test('should render a row', () => {
    const mockTestWords = { 
      guessedWords: [{ guessedWord: 'train', letterMatchCount: 3 }]
    }

    const wrapper = shallow(<GuessingResults guessedWords={ mockTestWords.guessedWords }/>);
    expect(wrapper.find(`[data-test='guessed-word-row']`).length).toBe(1);  
    expect(wrapper.find(`[data-test='guessed-word-col']`).length).toBe(1);
    expect(wrapper.find(`[data-test='letterMatchCount-col']`).length).toBe(1);  
  });
});
