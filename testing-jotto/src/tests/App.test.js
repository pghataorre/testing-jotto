import React from 'react';
import { createStore } from 'redux';
import { shallow } from 'enzyme';
import App from '../App';
import rootReducer from '../reducers/rootReducer';
import initialState from '../initialState/initialState';

let store, wrapper;

const setUp = (state) => {
  store = createStore(rootReducer, state);
  wrapper = shallow(<App store={store} />);

  return wrapper;
}

beforeEach(() => {
  wrapper = setUp(initialState);
})

test('App renders', () => {
  // console.log(`\n  ================================= \n  wrapper === ${wrapper.dive().debug()} \n ================================= \n`);
  // expect(wrapper.dive().find(`[data-test='app-render']`).length).toBe(1)
});
