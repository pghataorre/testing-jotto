import React from 'react';
import { createStore } from 'redux';
import { shallow, mount } from 'enzyme';
import PlayAgain from '../components/playAgain';
import rootReducer from '../reducers/rootReducer';
import initialState from '../initialState/initialState';
import sinon from 'sinon';

let wrapper, store;

const setUp = (state, useMount) => {
  store = createStore(rootReducer, state);
  wrapper = useMount ? mount(<PlayAgain store={store}/>) : shallow(<PlayAgain store={store}/>);
  return wrapper;
}

describe('Play Again Component', () => {
  test('should render with NO Button', () => {
    wrapper = setUp(initialState).dive().dive();
    expect(wrapper.find(`[data-test='play-again-button']`).length).toBe(0);
  });

  test('should render with NO Button', () => {

    const mockState = {
      successReducer: {
        success: true,
        guessedWords: [],
        wordToGuess: 'testing'
      }
    };

    wrapper = setUp(mockState).dive().dive();
    expect(wrapper.find(`[data-test='play-again-button']`).length).toBe(1);
  });

  test('should click Button', () => {
    const mockReplayGame = sinon.spy(wrapper.instance(), 'replayGame');
    const mockState = {
      successReducer: {
        success: true,
        guessedWords: [],
        wordToGuess: 'testing'
      }
    };

    const store = createStore(rootReducer, mockState);
    wrapper = mount(<PlayAgain store={store} replayGame={mockReplayGame} />);
    const button = wrapper.find(`[data-test='play-again-button']`);
    // console.log(`\n  ================================= \n button === ${button.debug()} \n ================================= \n`);
    button.simulate('click');

    // expect(mockReplayGame).toHaveBeenCalledTimes(1);
  });
});
