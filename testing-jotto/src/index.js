import React from 'react';
import ReactDOM from 'react-dom'
import App from './App';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import initialState from './initialState/initialState';
import rootReducer from './reducers/rootReducer';
import './index.css';

const  middleWares = [thunk];
const store = createStore(rootReducer, initialState, applyMiddleware(...middleWares));
store.subscribe(() => {
  // console.log(`\n  ================================= \n  ${JSON.stringify(store.getState())} \n ================================= \n`);
});

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
