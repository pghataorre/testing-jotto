const initialState = {
  success: false,
  guessedWords: [],
  wordToGuess: 'testing',
  asterisksDisplay: ''
};

export default initialState;