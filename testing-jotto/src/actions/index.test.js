import { createStore, applyMiddleware } from 'redux';
import initialState from '../initialState/initialState';
import rootReducer from '../reducers/rootReducer';
import thunk from 'redux-thunk';
import {correctGuess, incorrectGuess, setAttemptGuess, actionTypes, replayGameAction, getSecretWord, setDefaultAsterisks} from './index';
import moxios from 'moxios';

let store;

const setUpStore = () => {
  const middleWares = [thunk];
  store = createStore(rootReducer, initialState,  applyMiddleware(...middleWares));
}

describe.only('Action creators', () => {
  test('Test correctGuess() returns an action with type CORRECT_GUESS', () => {
    const output = correctGuess();

    expect(output).toEqual({type: actionTypes.CORRECT_GUESS })
  });

  test('Test incorrectGuess() returns an action with type CORRECT_GUESS', () => {
    const output = incorrectGuess('WRONGWORD', 4);
    const mockOutputResult = {
      type: actionTypes.INCORRECT_GUESS,
      payload: {
        guessWord: 'WRONGWORD',
        wordCount: 4
      }
    };

    expect(output).toEqual(mockOutputResult);
  });

  test('Test setAttemptGuess() with INCORRECT guess', () => {
    const testWrongGuessWord = 'testi';
    setUpStore();

    store.dispatch(setAttemptGuess(testWrongGuessWord));
    const newState = store.getState().failedReducer.guessedWords;
    const expectedOutput = [{"guessedWord":"testi","letterMatchCount":4}] ;

    expect(newState).toEqual(expectedOutput);
  });

  test('Test setAttemptGuess() with CORRECT guess', () => {
    const correctTestWord = 'testing';
    setUpStore();
    store.dispatch(setAttemptGuess(correctTestWord));
    const newState = store.getState().successReducer.success;
    const expectedOutput = true;
    
    expect(newState).toEqual(expectedOutput);
  });

  test('Test setDefaultAsterisks()', () => {
    const secretWord = '*******';
    setUpStore();
    store.dispatch(setDefaultAsterisks(secretWord));
    const newState = store.getState().successReducer.asterisksDisplay;
    const expectedOutput = {
      asterisksDisplay: '*******'
    };

    expect(newState).toBe(expectedOutput.asterisksDisplay);
  });

  test('Test getSecretWord()', () => {
    const secretWord = 'testing';
    moxios.install();
    setUpStore();
    
    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 200,
        response: {
          secret: secretWord
        }
      })
    });

    return store.dispatch(getSecretWord()).then(() => {
      const newState = store.getState().successReducer;
      expect(newState.wordToGuess).toBe(secretWord);
    });
  });
});
