import { getLetterMatchCount, setAsterisksLeters } from '../helpers/index'; 
import axios from 'axios';
 
export const actionTypes = {
  CORRECT_GUESS: 'CORRECT_GUESS',
  INCORRECT_GUESS: 'INCORRECT_GUESS',
  REPLAY_GAME: 'REPLAY_GAME',
  SECRET_WORD: 'SECRET_WORD',
  SET_ASTERISKS_WORD: 'SET_ASTERISKS_WORD'
};

function setAsterisksText(wordToGuess) {
  return { type: actionTypes.SET_ASTERISKS_WORD, payload: { wordToGuess } };
}

export function setSecretWord(secretResponse) {
  return {type: actionTypes.SECRET_WORD, payload: {secretResponse: secretResponse.secret}};
}

export function correctGuess() {
  return {type: actionTypes.CORRECT_GUESS};
}

export function replayReducer() {
  return {type: actionTypes.REPLAY_GAME};
}

export function incorrectGuess(guessWord, wordCount) {
  return {
    type: actionTypes.INCORRECT_GUESS,
    payload: {guessWord, wordCount}
  }
}

export function setAttemptGuess(guessWord) {
  return (dispatch, getState) => {
    const secretWord = getState().successReducer.wordToGuess;
      
    if (guessWord === secretWord) {
      dispatch(correctGuess());
    } else {
      const wordCount = getLetterMatchCount(guessWord, secretWord);
      const newAsterisksLetters = setAsterisksLeters(guessWord, secretWord);
      dispatch(setAsterisksText(newAsterisksLetters));
      dispatch(incorrectGuess(guessWord, wordCount));
    }
  }
}

export const replayGameAction = () => {
  return (dispatch) => {
    dispatch(replayReducer());
  }
}

export const getSecretWord = () => {
  return (dispatch) => {
    return axios.get('http://localhost:3333/get-secret-word')
      .then((response) => {
        let secretWordResponse = JSON.parse(response.data);
        dispatch(setSecretWord(secretWordResponse));
      })
      .catch((error) => {
        console.log('ERROR');
      });
  }
}

export const setDefaultAsterisks = (wordToGuess) => {
  return (dispatch) => {
    return dispatch({ type: actionTypes.SET_ASTERISKS_WORD, payload: { wordToGuess } });
  }
}
