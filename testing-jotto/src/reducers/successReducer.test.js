import { actionTypes } from '../actions/index';
import initialState from '../initialState/initialState';
import successReducer from '../reducers/successReducer';

describe('Test success reducer', () => {
  test('Test success reducer with NO ACTION TYPE', () => {
    const mockAction = {
      type: ''
    };

    const output = successReducer(initialState, mockAction);
    expect(output).toEqual(initialState)
  });

  test('Test success reducer CORRECT_GUESS', () => {
    const mockOutputState = {
      success: true,
      guessedWords: [],
      wordToGuess: 'testing',
      asterisksDisplay: ''
    };

    const mockAction = {
      type: actionTypes.CORRECT_GUESS
    }

    const output = successReducer(initialState, mockAction);
    expect(output).toEqual(mockOutputState)
  });
});