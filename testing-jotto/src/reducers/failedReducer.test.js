import { actionTypes } from '../actions/index';
import initialState from '../initialState/initialState';
import failedReducer from '../reducers/failedReducer';

describe('Test failedReducer', () => {
  test('Test success reducer with NO ACTION TYPE', () => {
    const mockAction = {
      type: ''
    };

    const output = failedReducer(initialState, mockAction);
    expect(output).toEqual(initialState);
  });

  test('Test failedReducer INCORRECT_GUESS', () => {
    const mockAction = {
      type: actionTypes.INCORRECT_GUESS,
      payload: {
        guessWord: 'TEST', 
        wordCount: 4
      }
    };

    const mockOutputState = {
      success: false,
      guessedWords: [{guessedWord: 'TEST', letterMatchCount: 4}],
      wordToGuess: 'testing',
      "asterisksDisplay": ""
    };

    const output = failedReducer(initialState, mockAction);
    expect(output).toEqual(mockOutputState);
  });

  test('Test failedReducer REPLAY_GAME:', () => {
    const mockAction = {
      type: actionTypes.REPLAY_GAME
    };

    const mockInputState = {
      success: false,
      guessedWords: [],
      wordToGuess: 'testing'
    };

    const mockOutputState = failedReducer(mockInputState, mockAction);

    expect(mockOutputState).toEqual(mockOutputState);
  });
});
