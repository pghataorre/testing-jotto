import initialState from "../initialState/initialState";
import  { actionTypes } from '../actions/index';

export default function failedReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.REPLAY_GAME:
      return {
        ...state,
        guessedWords: []
      }

    case actionTypes.INCORRECT_GUESS:
      const guessEntry = { 
        guessedWord: action.payload.guessWord, 
        letterMatchCount: action.payload.wordCount
      };

      return {
        ...state,
        guessedWords: [...state.guessedWords, guessEntry]
      };

    default:

      return state;
  }
}
