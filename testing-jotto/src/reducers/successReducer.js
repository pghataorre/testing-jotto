import initialState from "../initialState/initialState";
import  { actionTypes } from '../actions/index';

export default function successReducer (state = initialState, action) {
  switch(action.type) {
    case actionTypes.SET_ASTERISKS_WORD:
      return {
        ...state,
        asterisksDisplay: action.payload.wordToGuess
      }

    case actionTypes.REPLAY_GAME:
      return {
        ...state,
        success: false
      }

    case actionTypes.CORRECT_GUESS: 
      return Object.assign({}, state, {
        success: true,
        asterisksDisplay: ''
      });

    case actionTypes.SECRET_WORD:  
      return {
        ...state,
        wordToGuess: action.payload.secretResponse
      }

    default:

      return state;
  }
}
