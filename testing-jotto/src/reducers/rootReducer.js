import { combineReducers } from 'redux';
import successReducer from './successReducer';
import failedReducer from './failedReducer';

const rootReducer = combineReducers({
  successReducer,
  failedReducer
});

export default rootReducer;
