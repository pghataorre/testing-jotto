import {getLetterMatchCount, setAsterisksLeters, convertToAsterisks} from './index';

describe('Test getLetterMatchCount()', () => {
  const secretWord = 'Lucky';

  test('should Not match secret word', () => {
    const guessedWord = 'train';
    const letterMatchCount = getLetterMatchCount(guessedWord, secretWord)

    expect(letterMatchCount).toBe(0);
  });

  test('should match SOME letters in secret word', () => {
    const guessedWord = 'Duck';
    const letterMatchCount = getLetterMatchCount(guessedWord, secretWord);
    expect(letterMatchCount).toBe(3);
  });

  test('should match FIRST letter in secret word', () => {
    const guessedWord = 'London';
    const letterMatchCount = getLetterMatchCount(guessedWord, secretWord);
    expect(letterMatchCount).toBe(1);
  });

  test('should match ALL in secret word', () => {
    const guessedWord = 'Lucky';
    const letterMatchCount = getLetterMatchCount(guessedWord, secretWord);
    expect(letterMatchCount).toBe(5);
  });

  test('should return 0 when no guessed word is added', () => {
    const guessedWord = '';
    const letterMatchCount = getLetterMatchCount(guessedWord, secretWord);
    expect(letterMatchCount).toBe(0);
  });
});

describe('Test setAsterisksLeters()', () => {
  test(`Should return Secrect word with some letters replaced by '*' `, () => {
    const guesedWord = 'Orange';
    const secretWord = 'testing';
    const resultText = setAsterisksLeters(guesedWord, secretWord);
    const expecteedResult = '*e***ng';

    expect(expecteedResult).toBe(resultText);
  });

  test(`Should return Secrect word with all charachters NO '*'`, () => {
    const guesedWord = 'testing';
    const secretWord = 'testing';
    const resultText = setAsterisksLeters(guesedWord, secretWord);
    const expecteedResult = 'testing';

    expect(expecteedResult).toBe(resultText);
  });

  test(`Should return Secrect word with letters matched with ALL letters as '*' `, () => {
    const guesedWord = 'badword';
    const secretWord = 'testing';
    const resultText = setAsterisksLeters(guesedWord, secretWord);
    const expectedResult = '*******';

    expect(expectedResult).toBe(resultText);
  });
});

describe('Test convertToAsterisks()', () => {
  test(`Should return the secretWord back with '*' `, () => {
    const secretWord = 'testing';
    const result = convertToAsterisks(secretWord);
    const expectedOutput = '*******';

    expect(result).toBe(expectedOutput);
  });
});