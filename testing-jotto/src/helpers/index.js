export function getLetterMatchCount(guessedWord, secretWord) {
  const secretLetterSet = new Set(secretWord.split(''));
  const guessedLetterSet = new Set(guessedWord.split(''))

  if (guessedWord === '') {
    return 0;

  } else {
    return [...secretLetterSet].filter((letterItem) => {
       return guessedLetterSet.has(letterItem);
    }).length;
  }
}

export const setAsterisksLeters = (guessedWord, secretWord) => {
  const secretWordLettersArray = secretWord.split('');
  let fullWord = '';
  if (!guessedWord) return convertToAsterisks(secretWordLettersArray);

  const guessedWordLettersArray =  guessedWord.split('');
  const matchingLetters = guessedWordLettersArray.filter((item) => {
    return secretWordLettersArray.includes(item.toString()) ? item : null
  });

  secretWordLettersArray.map((item) => {
    let i = 0;
    let letter = '';
    let matchingLettersLength = matchingLetters.length;

    for(i; i < matchingLettersLength; i++) {
      if (item === matchingLetters[i]) {
        letter = item
        break;
      }
    };

    fullWord += letter === '' ? '*' : letter;
  });

  return fullWord;
}

export const convertToAsterisks = (secretWord) => {
  const secretWordArray = typeof secretWord === "string" ? secretWord.split('') :  secretWord;
  const asterisks = '*';
  return secretWordArray.map(() => {
    return asterisks;
  }).join('');
}